package com.cars24.actions;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;

import com.cars24.base.*;

public class LoginScreenActions extends BaseClass{

	WebDriver driver;
	public LoginScreenActions(WebDriver driver){
		super(driver);
		driver = this.driver;
	}
	
	@FindBy(id="com.cars24.inspection:id/etUserName")
	private WebElement inp_username;
	
	@FindBy(id="com.cars24.inspection:id/etPassword")
	private WebElement inp_password;
	
	@FindBy(id="com.cars24.inspection:id/btnSubmit")
	private WebElement btn_submit;
	
	@FindBy(id="com.cars24.inspection:id/logo")
	private WebElement logo;
	
	public void loginToTheApplication(String username, String password){
		inp_username.sendKeys(username);
		Reporter.log("User enters "+username+" as username", true);
		logo.click();
		inp_password.sendKeys(password);
		Reporter.log("User enters "+password+" as password", true);
		logo.click();
		btn_submit.click();
		Reporter.log("User clicked on Login button", true);
	}

	public void verifyUserIsOnLoginScreen() {
		Assert.assertTrue(logo.isDisplayed(),"Logo is not displayed - User is not on login page");
		Reporter.log("User is on Login Screen", true);
	}
}
