package com.cars24.actions;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;

import com.cars24.base.BaseClass;

public class MainScreenActions extends BaseClass{
	
	WebDriver driver;
	
	public MainScreenActions(WebDriver driver){
		super(driver);
		driver = this.driver;
	}

	@FindBy(id="com.cars24.inspection:id/btn_start_inspection")
	WebElement btn_startInspection;
	
	@FindBy(id="com.cars24.inspection:id/iv_logout")
	WebElement btn_logout;
	
	@FindBy(id="com.cars24.inspection:id/btn_positive")
	WebElement btn_confirmLogout;
	
	public void verifySuccessfulLogin() {
		Assert.assertTrue(btn_startInspection.isDisplayed(),"Assertion failed : User is not on Main Screen");
		Reporter.log("Assertion passed : User is successfully logged in", true);
	}

	public void logOut() {
		btn_logout.click();
		Reporter.log("User clicked on Log out button", true);
		btn_confirmLogout.click();
		Reporter.log("User clicked on Confirm logout button", true);
	}

}
