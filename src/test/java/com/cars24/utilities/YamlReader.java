package com.cars24.utilities;

import static com.cars24.utilities.ConfigReader.getProperty;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.util.Map;

import org.yaml.snakeyaml.Yaml;

import org.testng.Reporter;

@SuppressWarnings("unchecked")
public class YamlReader {

    public static String yamlFilePath = "src/test/resources/testData/QA_TestData.yaml";

    public static String setYamlFilePath() {
    	String tier = System.getProperty("env");
    	if (tier == null) tier = getProperty("Config.properties", "tier").trim();
        System.out.println("The Tier is :- " + tier);
        if (tier.equalsIgnoreCase("qa")) {
            yamlFilePath = "src/test/resources/testData/QA_TestData.yaml";
        } else if (tier.equalsIgnoreCase("stg")) {
            yamlFilePath = "src/test/resources/testData/STG_TestData.yaml";
        } else if (tier.equalsIgnoreCase("prod")) {
            yamlFilePath = "src/test/resources/testData/PROD_TestData.yaml";
        } else {
            Reporter.log("YOU HAVE PROVIDED WRONG TIER IN CONFIG!!! using mice test data", true);
        }
        System.out.println("The YAML file path is:- " + yamlFilePath);
        return yamlFilePath;
    }

    public static String getYamlValue(String token) {
        try {
            return getValue(token);
        } catch (FileNotFoundException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    public static String getData(String token) {
        return getYamlValue(token);
    }

    public static Map<String, Object> getYamlValues(String token) {
        Reader doc;
        try {
            doc = new FileReader(yamlFilePath);
        } catch (FileNotFoundException ex) {
            System.out.println("File not valid or missing!!!");
            ex.printStackTrace();
            return null;
        }
        Yaml yaml = new Yaml();
        // TODO: check the type casting of object into the Map and create
        // instance in one place
        Map<String, Object> object = (Map<String, Object>) yaml.load(doc);
        return parseMap(object, token + ".");
    }

    private static String getValue(String token) throws FileNotFoundException {
        Reader doc = new FileReader(yamlFilePath);
        Yaml yaml = new Yaml();
        Map<String, Object> object = (Map<String, Object>) yaml.load(doc);
        return getMapValue(object, token);

    }

    public static String getMapValue(Map<String, Object> object, String token) {
        // TODO: check for proper yaml token string based on presence of '.'
        String[] st = token.split("\\.");
        return parseMap(object, token).get(st[st.length - 1]).toString();
    }

    private static Map<String, Object> parseMap(Map<String, Object> object,
            String token) {
        if (token.contains(".")) {
            String[] st = token.split("\\.");
            object = parseMap((Map<String, Object>) object.get(st[0]),
                    token.replace(st[0] + ".", ""));
        }
        return object;
    }
}