package com.cars24.base;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

public class BaseClass {
	WebDriver driver;
	public BaseClass(WebDriver driver){
		PageFactory.initElements(driver, this);
		driver = this.driver;
	}

	public void scrollToElement(WebElement element){
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scroll(0,0)");
	}
}
