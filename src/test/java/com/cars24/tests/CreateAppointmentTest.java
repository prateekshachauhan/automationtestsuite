package com.cars24.tests;

import static com.cars24.utilities.YamlReader.getData;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.cars24.sessionInitiator.TestSessionInitiator;

public class CreateAppointmentTest {

	TestSessionInitiator test;
	@BeforeTest
	public void setUp() {
		test=new TestSessionInitiator();
	}

	@Test
	public void Step01_VerifyLoginScreenIsDisplayed() {
		test.stepStartMessage("Step01_VerifyLoginScreenIsDisplayed");
		test.loginScreen.verifyUserIsOnLoginScreen();
	}
	
	@Test(dependsOnMethods="Step01_VerifyLoginScreenIsDisplayed")
	public void Step02_LoginToTheAplication(){
		test.stepStartMessage("Step02_LoginToTheAplication");
		test.loginScreen.loginToTheApplication(getData("inspection.username"),getData("inspection.password"));
		test.mainScreen.verifySuccessfulLogin();
	}
	
	@Test(dependsOnMethods="")
	public void Step03_ClickOnCreateAppointmentButton(){
	//test.mainScreen.clickOnCreateAq	
	}
	
}
