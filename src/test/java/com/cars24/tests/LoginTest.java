package com.cars24.tests;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import static com.cars24.utilities.YamlReader.getData;

import com.cars24.sessionInitiator.TestSessionInitiator;

public class LoginTest {
	TestSessionInitiator test;
	@BeforeTest
	public void setUp() {
		test=new TestSessionInitiator();
	}

	@Test
	public void Step01_VerifyLoginScreenIsDisplayed() {
		test.stepStartMessage("Step01_VerifyLoginScreenIsDisplayed");
		test.loginScreen.verifyUserIsOnLoginScreen();
	}
	
	@Test(dependsOnMethods="Step01_VerifyLoginScreenIsDisplayed")
	public void Step02_LoginToTheAplication(){
		test.stepStartMessage("Step02_LoginToTheAplication");
		test.loginScreen.loginToTheApplication(getData("inspection.username"),getData("inspection.password"));
		test.mainScreen.verifySuccessfulLogin();
	}
	
	@Test(dependsOnMethods="Step02_LoginToTheAplication")
	public void Step03_LogOutOfTheApplication(){
		test.stepStartMessage("Step03_LogOutOfTheApplication");
		test.mainScreen.logOut();
		test.loginScreen.verifyUserIsOnLoginScreen();
	}
	
	@AfterTest
	public void tearDown(){
		test.closeSession();
	}
}
