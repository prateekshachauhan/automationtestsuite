package com.cars24.sessionInitiator;

import static com.cars24.utilities.YamlReader.setYamlFilePath;
import static com.cars24.utilities.YamlReader.getYamlValue;
import static com.cars24.utilities.ConfigReader.getProperty;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;

import java.util.HashMap;

import org.testng.Reporter;

import com.cars24.actions.LoginScreenActions;
import com.cars24.actions.MainScreenActions;

public class TestSessionInitiator {

	protected WebDriver driver;
	private WebDriverFactory wdfactory;
	Map<String, Object> chromeOptions = null;
	public LoginScreenActions loginScreen;
	public MainScreenActions mainScreen;
	
	private void _initPage() {
		loginScreen = new LoginScreenActions(driver);
		mainScreen = new MainScreenActions(driver);
	}
		

	public TestSessionInitiator() {
		wdfactory = new WebDriverFactory();
		setYamlFilePath();
		_configureBrowser();
		_initPage();
	}

	private void _configureBrowser() {
		driver = wdfactory.getDriver(_getSessionConfig());
		driver.manage().window().maximize();
		driver.manage()
				.timeouts()
				.implicitlyWait(
						Integer.parseInt(_getSessionConfig().get("timeout")),
						TimeUnit.SECONDS);
	}

	private Map<String, String> _getSessionConfig() {
		String[] configKeys = { "tier", "browser", "seleniumserver",
				"seleniumserverhost", "timeout"};
		Map<String, String> config = new HashMap<String, String>();
		for (String string : configKeys) {
			config.put(string, getProperty("./Config.properties", string));
		}
		return config;
	}
	
	public String getEnv(){
		return _getSessionConfig().get("tier");
	}
	
	public void launchApplication() {
		launchApplication(getYamlValue("app_url"));
	}

	public void launchApplication(String applicationpath) {
		Reporter.log("The application url is :- " + applicationpath, true);
		Reporter.log(
				"The test browser is :- " + _getSessionConfig().get("browser"),
				true);
		driver.get(applicationpath);
	}

	public void getURL(String url) {
		driver.manage().deleteAllCookies();
		driver.get(url);
	}

	public void closeSession() {
		driver.quit();
	}
	
	public void stepStartMessage(String testStepName){
		Reporter.log(" ", true);
		Reporter.log("***** STARTING TEST STEP:- " + testStepName.toUpperCase() + " *****", true);
		Reporter.log(" ", true);
	}

}